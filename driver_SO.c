#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/timer.h>
#include <linux/init.h>
#include <linux/miscdevice.h> // misc dev
#include <linux/fs.h>         // file operations
#include <asm/uaccess.h>      // copy to/from user space
#include <linux/wait.h>       // waiting queue
#include <linux/sched.h>      // TASK_INTERRUMPIBLE
#include <linux/delay.h>      // udelay

#include <linux/interrupt.h>
#include <linux/gpio.h>

#include <linux/proc_fs.h>

#define DRIVER_AUTHOR "Andres Rodriguez - DAC"
#define DRIVER_DESC   "Ejemplo Driver para placa lab. DAC Rpi"

#define GPIO_BUTTON1_DESC "Button 1"
#define GPIO_BUTTON2_DESC "Button 2"

#define GPIO_BUTTON1_DEVICE_DESC "Increment value"
#define GPIO_BUTTON2_DEVICE_DESC "Decrement value"

//SPEAKER

#define DEVICE "/dev/speaker"
#define LOW_LEVEL '0'
#define HIGH_LEVEL '1'

//GPIOS numbers as in BCM RPi

#define GPIO_BUTTON1 2
#define GPIO_BUTTON2 3

#define GPIO_SPEAKER 4

#define GPIO_GREEN1  27
#define GPIO_GREEN2  22
#define GPIO_YELLOW1 17
#define GPIO_YELLOW2 11
#define GPIO_RED1    10
#define GPIO_RED2    9

#define BUFFER_SIZE 1024

//static struct proc_dir_entry *proc_entry_leds;
//static struct proc_dir_entry *proc_entry_speaker;

static int LED_GPIOS[]= {GPIO_GREEN1, GPIO_GREEN2, GPIO_YELLOW1, GPIO_YELLOW2, GPIO_RED1, GPIO_RED2} ;

static char *led_desc[]= {"GPIO_GREEN1","GPIO_GREEN2","GPIO_YELLOW1","GPIO_YELLOW2","GPIO_RED1","GPIO_RED2"} ;

static unsigned int irq_BUTTON1 = 0;
static unsigned int irq_BUTTON2 = 0;

static irqreturn_t r_irq_handler_button1(void);
static irqreturn_t r_irq_handler_button2(void);

/* speaker buffer */
static char *read_buf=NULL;

/* buttons management */
static char *read_buttons_buf=NULL;
static DECLARE_WAIT_QUEUE_HEAD(wait_queue);
static DEFINE_SEMAPHORE(sem);
//struct semaphore sem;
static int write_p = 0;
static int read_p = 0;

/****************************************************************************/
/* LEDs write/read using gpio kernel API                                    */
/****************************************************************************/

static void byte2leds(char ch)
{
    int i;
    int val=(int)ch;

    for(i=0; i<6; i++) gpio_set_value(LED_GPIOS[i], (val >> i) & 1);
}

static char leds2byte(void)
{
    int val;
    char ch;
    int i;
    ch=0;

    for(i=0; i<6; i++)
    {
        val=gpio_get_value(LED_GPIOS[i]);
        ch= ch | (val << i);
    }
    return ch;
}

/****************************************************************************/
/* LEDs device file operations                                              */
/****************************************************************************/

static ssize_t leds_write(struct file *file, const char __user *buf,
                          size_t count, loff_t *ppos)
{
	int ch;
	
	if (copy_from_user(read_buf, buf, count))  return -EFAULT;
	read_buf[count+1] = '\0';
	
	//if (!sscanf(read_buf, "%d", &ch)) return -EFAULT;
	
	ch=(int)read_buf[0];
	
	printk( KERN_INFO " (write) valor recibido: %d\n",(int)ch);
	
	
	if(ch < 64){
		byte2leds(ch);
	} else if(ch < 128){
		byte2leds(leds2byte() | ch);
	} else if(ch < 192){
		byte2leds(leds2byte() ^ ch);
	} else {
		return -EFAULT;
	}
    
    return 1;
}

static ssize_t leds_read(struct file *file, char __user *buf,
                         size_t count, loff_t *ppos)
{
    char ch;

    if(*ppos==0) *ppos+=1;
    else return 0;

    ch=leds2byte();

    printk( KERN_INFO " (read) valor entregado: %d\n",(int)ch);


    if(copy_to_user(buf,&ch,1)) return -EFAULT;

    return 1;
}

/****************************************************************************/
/* SPEAKER                                         */
/****************************************************************************/

static ssize_t speaker_write(struct file *file, const char __user *buf,
                          size_t count, loff_t *ppos)
{
	int ch;
	
	if (copy_from_user(read_buf, buf, count))  return -EFAULT;
	read_buf[count+1] = '\0';
	
	//if (!sscanf(read_buf, "%d", &ch)) return -EFAULT;
	
	ch=(int)(read_buf[0]=='1');
	
	gpio_set_value(GPIO_SPEAKER, ch);

    printk( KERN_INFO " (write) valor recibido: %d\n",(int)ch);
	
    return 1;
}

/****************************************************************************/
/* BUTTONS                                         							*/
/****************************************************************************/

static ssize_t scull_p_read (struct file *filp, char __user *buf, size_t count, loff_t *f_pos){
	int cnt = 0;
	
	if (down_interruptible(&sem)){
        return -ERESTARTSYS;
	}
    while (read_p == write_p) { /* nothing to read */
		up(&sem); /* release the lock */
		if (wait_event_interruptible(wait_queue, (read_p != write_p)))
            return -ERESTARTSYS; /* signal: tell the fs layer to handle it */
        /* otherwise loop, but first reacquire the lock */
        if (down_interruptible(&sem)){
			return -ERESTARTSYS;
		}
	}
	
	read_buttons_buf[++write_p] = '\0';
	/* ok, data is there, return something */
	if (write_p > read_p){
        cnt = min(count, (size_t) (write_p - read_p));
	}else {/* the write pointer has wrapped, return data up to dev->end */
        cnt = min(count, (size_t) (BUFFER_SIZE - read_p));
	}
	
	if (copy_to_user(buf, &read_buttons_buf[read_p], cnt)) {
        up(&sem);
        return -EFAULT;
    }
    
    if(read_p + cnt > BUFFER_SIZE){
		read_p = cnt;
	} else {	
		read_p += cnt;
	}
	
	up (&sem);
	
	wake_up_interruptible(&wait_queue);
    
    return cnt;
}

static const struct file_operations leds_fops = {
    .owner	= THIS_MODULE,
    .write	= leds_write,
    .read	= leds_read,
};

static const struct file_operations speaker_fops = {
    .owner	= THIS_MODULE,
    .write	= speaker_write,
};

static const struct file_operations buttons_fops = {
    .owner	= THIS_MODULE,
    .read	= scull_p_read,
};


/****************************************************************************/
/* Buttons requests                                            */
/****************************************************************************/
static int r_int_BUTTON_config(unsigned GPIO_BUTTON, char * GPIO_BUTTON_DESC, char * GPIO_BUTTON_DEVICE_DESC, irqreturn_t (*r_irq_handler)(void), unsigned *irq_BUTTON)
{
	int res=0;
    if ((res=gpio_request(GPIO_BUTTON, GPIO_BUTTON_DESC))) {
        printk(KERN_ERR "GPIO request faiure: %s\n", GPIO_BUTTON_DESC);
        return res;
    }

    if ( (*irq_BUTTON = gpio_to_irq(GPIO_BUTTON)) < 0 ) {
        printk(KERN_ERR "GPIO to IRQ mapping faiure %s\n", GPIO_BUTTON_DESC);
        
        return *irq_BUTTON;
    }

    printk(KERN_NOTICE "  Mapped int %d for button1 in gpio %d\n", *irq_BUTTON, GPIO_BUTTON);

    if ((res=request_irq(*irq_BUTTON,
                    (irq_handler_t ) r_irq_handler,
                    IRQF_TRIGGER_FALLING,
                    GPIO_BUTTON_DESC,
                    GPIO_BUTTON_DEVICE_DESC))) {
        printk(KERN_ERR "Irq Request failure\n");
        return res;
    }
    
    return res;
}

static void f_timerHandler1(unsigned long data){
	enable_irq(data);
}

DEFINE_TIMER(timer_handler1, f_timerHandler1, 0, 0);
DEFINE_TIMER(timer_handler2, f_timerHandler1, 0, 0);

static int r_int_BUTTONS_config(void){
	int res=0;

	/* configure button 1 */
	if((res = r_int_BUTTON_config(GPIO_BUTTON1, GPIO_BUTTON1_DESC, GPIO_BUTTON1_DEVICE_DESC, r_irq_handler_button1, &irq_BUTTON1))){
		printk(KERN_ERR "BUTTON1 request faiure: %s\n", GPIO_BUTTON1_DESC);
		return res;
	}
	timer_handler1.data = irq_BUTTON1;
	/* configure button 2 */
	if((res = r_int_BUTTON_config(GPIO_BUTTON2, GPIO_BUTTON2_DESC, GPIO_BUTTON2_DEVICE_DESC, r_irq_handler_button2, &irq_BUTTON2))){
		printk(KERN_ERR "BUTTON1 request faiure: %s\n", GPIO_BUTTON1_DESC);
		return res;
	}
	timer_handler2.data = irq_BUTTON2;
	
	return res;
}


/****************************************************************************/
/* LEDs device struct                                                       */

static struct miscdevice leds_miscdev = {
    .minor	= MISC_DYNAMIC_MINOR,
    .name	= "leds",
    .fops	= &leds_fops,
};

/* Speaker's device struct                                                       */

static struct miscdevice speaker_miscdev = {
    .minor	= MISC_DYNAMIC_MINOR,
    .name	= "speaker",
    .fops	= &speaker_fops,
};

/* Buttons' device struct                                                       */

static struct miscdevice buttons_miscdev = {
    .minor	= MISC_DYNAMIC_MINOR,
    .name	= "buttons",
    .fops	= &buttons_fops,
};

/*****************************************************************************/
/* This functions registers devices, requests GPIOs and configures interrupts */
/*****************************************************************************/

/*******************************
 *  register device for leds
 *******************************/

static int r_dev_config(void)
{
    int ret=0;
    printk(KERN_NOTICE "misc_register");
    ret = misc_register(&leds_miscdev);
    
    if (ret < 0) {
        printk(KERN_ERR "misc_register failed\n");
    }
	else
		printk(KERN_NOTICE "misc_register OK... leds_miscdev.minor=%d\n", leds_miscdev.minor);
		
	ret=0;
    ret = misc_register(&speaker_miscdev);
    if (ret < 0) {
        printk(KERN_ERR "misc_register failed\n");
    }
	else
		printk(KERN_NOTICE "misc_register OK... speaker_miscdev.minor=%d\n", speaker_miscdev.minor);
		
	ret=0;
    ret = misc_register(&buttons_miscdev);
    if (ret < 0) {
        printk(KERN_ERR "misc_register failed\n");
    }
	else
		printk(KERN_NOTICE "misc_register OK... speaker_miscdev.minor=%d\n", buttons_miscdev.minor);
		
	return ret;
}

/*******************************
 *  request and init gpios for leds
 *******************************/

static int r_GPIO_config(void)
{
    int i;
    int res=0;
    for(i=0; i<6; i++)
    {
        if ((res=gpio_request_one(LED_GPIOS[i], GPIOF_INIT_LOW, led_desc[i]))) 
        {
            printk(KERN_ERR "GPIO request faiure: led GPIO %d %s\n",LED_GPIOS[i], led_desc[i]);
            return res;
        }
        gpio_direction_output(LED_GPIOS[i],0);
	} 
	if ((res = r_int_BUTTONS_config())){
		printk(KERN_ERR "BUTTONS_config request faiure: led GPIO %d %s\n",LED_GPIOS[i], led_desc[i]);
            return res;
	}
	if((res = gpio_request_one(GPIO_SPEAKER, GPIOF_INIT_LOW, "SPEAKER"))){
		printk(KERN_ERR "GPIO request faiure: Speaker GPIO %d %s\n",GPIO_SPEAKER, "Speaker");
        return res;
	}
	
	return res;
}

/****************************************************************************/
/* Interruptions                                            				*/
/****************************************************************************/

static void f_buttonHalf1(unsigned long unused){
	wake_up_interruptible(&wait_queue);
}

static void f_buttonHalf2(unsigned long unused){
	wake_up_interruptible(&wait_queue);
}

DECLARE_TASKLET(buttonHalf1, f_buttonHalf1, 0);
DECLARE_TASKLET(buttonHalf2, f_buttonHalf2, 0);


static irqreturn_t r_irq_handler_button1(void){
	/* manage critic part of the interruption */
	
	disable_irq_nosync(irq_BUTTON1);
	mod_timer(&timer_handler1, jiffies + HZ/2); /* HZ = ticks in 1 second */

	down(&sem);
	//if (down(&sem))
    //    return -ERESTARTSYS;
        
    if(write_p > BUFFER_SIZE)
		write_p = 0;
		
    read_buttons_buf[write_p++] = '1';
    
    up(&sem);
    //if (up(&sem))
    //    return -ERESTARTSYS;

	tasklet_schedule(&buttonHalf1);  /* manage user's not critic part (not importrant data) */

	return IRQ_HANDLED;
}

static irqreturn_t r_irq_handler_button2(void){
	/* manage critic part of the interruption */
	
	disable_irq_nosync(irq_BUTTON2);
	mod_timer(&timer_handler2, jiffies + HZ/2); /* HZ = ticks in 1 second */

	down(&sem);
	//if (down(&sem))
    //    return -ERESTARTSYS;
        
    if(write_p > BUFFER_SIZE)
		write_p = 0;
		
    read_buttons_buf[write_p++] = '2';
    
    up(&sem);
    //if (up(&sem))
    //    return -ERESTARTSYS;

	tasklet_schedule(&buttonHalf2);  /* manage user's not critic part (not importrant data) */

	return IRQ_HANDLED;
}


/****************************************************************************/
/* Module init / cleanup block.                                             */
/****************************************************************************/

static void r_cleanup(void) {
    int i;
    printk(KERN_NOTICE "%s module cleaning up...\n", KBUILD_MODNAME);
    
    for(i=0; i<6; i++)
        gpio_free(LED_GPIOS[i]);
        
    printk("devices desregistration...\n");
    
    if (leds_miscdev.this_device) 
    {
		printk("led dev desregistration...\n");
		misc_deregister(&leds_miscdev);
	}
    if (speaker_miscdev.this_device)
    {
		printk("speaker dev desregistration...\n");
		misc_deregister(&speaker_miscdev);
	}
    if (buttons_miscdev.this_device)
    {
		printk("buttons dev desregistration...\n");
    	 misc_deregister(&buttons_miscdev);
	 }
    
    if(irq_BUTTON1) free_irq(irq_BUTTON1, GPIO_BUTTON1_DEVICE_DESC);
	gpio_free(GPIO_BUTTON1);
	if(irq_BUTTON2) free_irq(irq_BUTTON2, GPIO_BUTTON2_DEVICE_DESC);
	gpio_free(GPIO_BUTTON2);
	
	gpio_free(GPIO_SPEAKER);
	
	/* clean buffers */
	if(read_buf) vfree(read_buf);
	if(read_buttons_buf) vfree(read_buttons_buf);
	
    printk(KERN_NOTICE "Done. Bye from %s module\n", KBUILD_MODNAME);
	
    return;
}

static int r_init(void) {
	int res=0;
    printk(KERN_NOTICE "Hello, loading %s module!\n", KBUILD_MODNAME);
    printk(KERN_NOTICE "%s - devices config...\n", KBUILD_MODNAME);
    read_buf = (char*) vmalloc(BUFFER_SIZE*sizeof(char));
    read_buttons_buf = (char*) vmalloc(BUFFER_SIZE*sizeof(char));
	
    if((res = r_dev_config()))
    {
		r_cleanup();
		return res;
	}
    printk(KERN_NOTICE "%s - GPIO config...\n", KBUILD_MODNAME);
    
    if((res = r_GPIO_config()))
    {
		r_cleanup();
		return res;
	}

    return res;
}

module_init(r_init);
module_exit(r_cleanup);

/****************************************************************************/
/* Module licensing/description block.                                      */
/****************************************************************************/
MODULE_LICENSE("GPL");
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
